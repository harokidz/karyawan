/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ukrim.model;

import com.ukrim.conn.Koneksi;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author harokidz
 */
public class Mobil {
    private String platNo;
    private String merek;
    private String warna;
    private String karyawanNik;
    private Karyawan karyawan;

    public Mobil(){
	
    }
    
    public Mobil(String platNo, String merek, String warna, String karyawanNik) {
	this.platNo = platNo;
	this.merek = merek;
	this.warna = warna;
	this.karyawanNik = karyawanNik;
    }

    
    public static ArrayList<Mobil> getAll(){
	ArrayList<Mobil> hasil = new ArrayList<>();
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	PreparedStatement ps = null;
	ResultSet r = null;
	String sql = "select * from mobil";
	try {
	    ps = c.prepareStatement(sql);
	    r = ps.executeQuery();
	    while(r.next()){
		Mobil m = new Mobil();
		m.platNo = r.getString("plat_no");
		m.merek = r.getString("merek");
		m.warna = r.getString("warna");
		m.karyawanNik = r.getString("karyawan_nik");
		m.karyawan = Karyawan.getByPrimaryKey(m.karyawanNik);
		hasil.add(m);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}finally{
	    k.tutupKoneksi(ps, r, c);
	}
	return hasil;
    }
    
    public static ArrayList<Mobil> getByKaryawanNik(String karyawanNik){
	ArrayList<Mobil> hasil = new ArrayList<>();
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	PreparedStatement ps = null;
	ResultSet r = null;
	String sql = "select * from mobil where karyawan_nik=?";
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, karyawanNik);
	    r = ps.executeQuery();
	    while(r.next()){
		Mobil m = new Mobil();
		m.platNo = r.getString("plat_no");
		m.merek = r.getString("merek");
		m.warna = r.getString("warna");
		m.karyawanNik = r.getString("karyawan_nik");
		m.karyawan = Karyawan.getByPrimaryKey(m.karyawanNik);
		hasil.add(m);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}finally{
	    k.tutupKoneksi(ps, r, c);
	}
	return hasil;
    }
    public static Mobil getByPrimaryKey(String platNo){
	Mobil m = null;
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	PreparedStatement ps = null;
	ResultSet r = null;
	String sql = "select * from mobil where plat_no=?";
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, platNo);
	    r = ps.executeQuery();
	    if(r.next()){
		m = new Mobil();
		m.platNo = r.getString("plat_no");
		m.merek = r.getString("merek");
		m.warna = r.getString("warna");
		m.karyawanNik = r.getString("karyawan_nik");
		m.karyawan = Karyawan.getByPrimaryKey(m.karyawanNik);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}finally{
	    k.tutupKoneksi(ps, r, c);
	}
	return m;
    }
    
    public void prosesTambah(){
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	PreparedStatement ps = null;
	String sql = "insert into mobil(plat_no,merek,warna,karyawan_nik) "
		+ "values (?,?,?,?)";
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, this.platNo);
	    ps.setString(2, this.merek);
	    ps.setString(3, this.warna);
	    ps.setString(4, this.karyawanNik);
	    ps.executeUpdate();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    public void prosesUbah(){
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	PreparedStatement ps = null;
	String sql = "update mobil set merek=?, warna=?, karyawan_nik=? "
		+ "where plat_no=?";
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, this.merek);
	    ps.setString(2, this.warna);
	    ps.setString(3, this.karyawanNik);
	    ps.setString(4, this.platNo);
	    ps.executeUpdate();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    public void prosesHapus(){
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	PreparedStatement ps = null;
	String sql = "delete from mobil where plat_no=?";
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, this.platNo);
	    ps.executeUpdate();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    
    public Karyawan getKaryawan() {
	return karyawan;
    }

    public void setKaryawan(Karyawan karyawan) {
	this.karyawan = karyawan;
    }

    
    public String getKaryawanNik() {
	return karyawanNik;
    }

    public void setKaryawanNik(String karyawanNik) {
	this.karyawanNik = karyawanNik;
    }

    public String getPlatNo() {
	return platNo;
    }

    public void setPlatNo(String platNo) {
	this.platNo = platNo;
    }

    public String getMerek() {
	return merek;
    }

    public void setMerek(String merek) {
	this.merek = merek;
    }

    public String getWarna() {
	return warna;
    }

    public void setWarna(String warna) {
	this.warna = warna;
    }
    
}
