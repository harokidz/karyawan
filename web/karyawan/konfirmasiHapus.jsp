<%-- 
    Document   : konfirmasiHapus
    Created on : Sep 13, 2018, 8:17:34 AM
    Author     : harokidz
--%>

<%@page import="com.ukrim.util.Util"%>
<%@page import="com.ukrim.model.Karyawan"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            String nik = request.getParameter("nik");
            Karyawan kr = Karyawan.getByPrimaryKey(nik);
            if (kr == null) {
                out.println("Data yang anda cari tidak ada");
                out.println("<br/><a href='index.jsp'>Kembali</a>");
                return;
            }
        %>
        <h1>Anda Yakin Hapus data ini?</h1>
        <p>Nik : <%=kr.getNik() %></p>
        <p>Nama : <%=kr.getNama() %></p>
        <p>Alamat : <%=kr.getAlamat() %></p>
        <p>Tanggal Lahir : <%=Util.konversiTanggal(kr.getTanggalLahir()) %></p>
        <p>Jenis Kelamin : <%=Util.konversiGendre(kr.getJenisKelamin()) %></p>
        <form method="get" action="prosesHapus.jsp">
            <input type="hidden" name="nik" value="<%=kr.getNik() %>"/>
            <input type="button" value="Batal" onclick="top.location.href='index.jsp'"/>
            <input type="submit" value="Hapus"/>
        </form>
    </body>
</html>
