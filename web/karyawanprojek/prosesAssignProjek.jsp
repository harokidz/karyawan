<%@page import="com.ukrim.model.ProjekKaryawan"%>
<%
    String nik = request.getParameter("nik");
    ProjekKaryawan.deleteByNik(nik);
    String[] projeks = request.getParameterValues("projeks");
    if (projeks != null) {
        for (int i = 0; i < projeks.length; i++) {
            ProjekKaryawan pk = new ProjekKaryawan();
            pk.setKaryawanNik(nik);
            pk.setProjekKode(projeks[i]);
            pk.setBonus(300000);
            pk.prosesTambah();
        }
    }
    response.sendRedirect("../index.jsp?action=assignProjek&nik=" + nik);
%>