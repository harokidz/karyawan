<%@page import="com.ukrim.model.Karyawan"%>
<%@page import="java.util.ArrayList"%>
<form method="get" action="mobil/prosesTambah.jsp">
    <div class="form-group">
        <label>Plat No</label>
        <input type="text" name="platNo" class="form-control" required/>
    </div>
    <div class="form-group">
        <label>Merek</label>
        <input type="text" name="merek" class="form-control" required/>
    </div>
    <div class="form-group">
        <label>Warna</label>
        <input type="color" name="warna" class="form-control" required/>
    </div>
    <div class="form-group">
        <label>Pemilik Mobil</label>
        <select name="karyawanNik" class="form-control">
            <%
                ArrayList<Karyawan> kList = Karyawan.getAll();
                for (Karyawan k : kList) {
            %>
            <option value="<%=k.getNik()%>">
                <%=k.getNik() %> | <%=k.getNama() %> | <%=k.getAlamat() %>

            </option>
            <%
                }
            %>
        </select>
    </div>
    <input type="submit" value="Simpan" class="btn btn-primary"/>
</form>