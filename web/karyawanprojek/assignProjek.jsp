<%@page import="com.ukrim.model.ProjekKaryawan"%>
<%@page import="com.ukrim.model.Projek"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ukrim.model.Karyawan"%>
<%
    String nik = request.getParameter("nik");
    Karyawan k = Karyawan.getByPrimaryKey(nik);
    if (k == null) {
        response.sendRedirect("index.jsp");
        return;
    }
    ArrayList<Projek> projekList = Projek.getAll();
    ArrayList<ProjekKaryawan> pkList = ProjekKaryawan.getByKaryawanNik(nik);
%>
<div class="row">
    <div class="col-12">
        <table class="table table-sm table-bordered">
            <tr>
                <td>NIK</td>
                <td><%=k.getNik()%></td>
            </tr>
            <tr>
                <td>Nama</td>
                <td><%=k.getNama()%></td>
            </tr>
        </table>
    </div>
    <div class="col-12">
        <form method="post" action="karyawanprojek/prosesAssignProjek.jsp">
            <table class="table table-sm table-bordered table-striped">
                <tr>
                    <th>Pilih</th>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>Biaya</th>
                </tr>
                <%
                    for (Projek p : projekList) {
                        String checked = "";
                        String color = "warning";
                        ProjekKaryawan pk = ProjekKaryawan.getByKaryawanNikAndProjekKode(k.getNik(), p.getKode());
                        if (pk != null) {
                            checked = "checked";
                            color = "success";
                        }

                %>
                <tr class="bg-<%=color%>">
                    <td><input type="checkbox" value="<%=p.getKode()%>" <%=checked%> name="projeks"/></td>
                    <td><%=p.getKode()%></td>
                    <td><%=p.getNama()%></td>
                    <td><%=p.getBiaya()%></td>
                </tr>
                <%
                    }
                %>
            </table>
            <input type="hidden" name="nik" value="<%=k.getNik()%>" />
            <input type="submit" class="btn btn-primary" value="Assign" />
        </form>
    </div>
</div>