/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ukrim.model;

import com.ukrim.conn.Koneksi;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author harokidz
 */
public class ProjekKaryawan {

    private int id;
    private String karyawanNik;
    private String projekKode;
    private int bonus;
    private Karyawan karyawan;
    private Projek projek;

    public static ArrayList<ProjekKaryawan> getAll() {
	ArrayList<ProjekKaryawan> hasil = new ArrayList<>();
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "select * from projek_karyawan";
	PreparedStatement ps = null;
	ResultSet r = null;
	try {
	    ps = c.prepareStatement(sql);
	    r = ps.executeQuery();
	    while (r.next()) {
		ProjekKaryawan pk = new ProjekKaryawan();
		pk.id = r.getInt("id");
		pk.karyawanNik = r.getString("karyawan_nik");
		pk.projekKode = r.getString("projek_kode");
		pk.karyawan = Karyawan.getByPrimaryKey(pk.karyawanNik);
		pk.projek = Projek.getByPrimaryKey(pk.projekKode);
		hasil.add(pk);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return hasil;
    }

    public static ArrayList<ProjekKaryawan> getByKaryawanNik(String karyawanNik) {
	ArrayList<ProjekKaryawan> hasil = new ArrayList<>();
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "select * from projek_karyawan where karyawan_nik=?";
	PreparedStatement ps = null;
	ResultSet r = null;
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, karyawanNik);
	    r = ps.executeQuery();
	    while (r.next()) {
		ProjekKaryawan pk = new ProjekKaryawan();
		pk.id = r.getInt("id");
		pk.karyawanNik = r.getString("karyawan_nik");
		pk.projekKode = r.getString("projek_kode");
		pk.karyawan = Karyawan.getByPrimaryKey(pk.karyawanNik);
		pk.projek = Projek.getByPrimaryKey(pk.projekKode);
		hasil.add(pk);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return hasil;
    }

    public static ArrayList<ProjekKaryawan> getByProjekKode(String projekKode) {
	ArrayList<ProjekKaryawan> hasil = new ArrayList<>();
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "select * from projek_karyawan where projek_kode=?";
	PreparedStatement ps = null;
	ResultSet r = null;
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, projekKode);
	    r = ps.executeQuery();
	    while (r.next()) {
		ProjekKaryawan pk = new ProjekKaryawan();
		pk.id = r.getInt("id");
		pk.karyawanNik = r.getString("karyawan_nik");
		pk.projekKode = r.getString("projek_kode");
		pk.karyawan = Karyawan.getByPrimaryKey(pk.karyawanNik);
		pk.projek = Projek.getByPrimaryKey(pk.projekKode);
		hasil.add(pk);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return hasil;
    }

    
    public static ProjekKaryawan getByKaryawanNikAndProjekKode(String karyawanNik, String projekKode) {
	ProjekKaryawan pk = null;
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "select * from projek_karyawan where karyawan_nik=? AND projek_kode=?";
	PreparedStatement ps = null;
	ResultSet r = null;
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, karyawanNik);
	    ps.setString(2, projekKode);
	    r = ps.executeQuery();
	    if (r.next()) {
		pk = new ProjekKaryawan();
		pk.id = r.getInt("id");
		pk.karyawanNik = r.getString("karyawan_nik");
		pk.projekKode = r.getString("projek_kode");
		pk.karyawan = Karyawan.getByPrimaryKey(pk.karyawanNik);
		pk.projek = Projek.getByPrimaryKey(pk.projekKode);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return pk;
    }
    
    public void prosesTambah(){
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "insert into projek_karyawan(karyawan_nik, projek_kode, bonus) "
		+ "values (?,?,?)";
	PreparedStatement ps = null;
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, this.karyawanNik);
	    ps.setString(2, this.projekKode);
	    ps.setInt(3, this.bonus);
	    ps.executeUpdate();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    
    public static void deleteByNik(String nik){
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "delete from projek_karyawan where karyawan_nik=?";
	PreparedStatement ps = null;
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, nik);
	    ps.executeUpdate();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getKaryawanNik() {
	return karyawanNik;
    }

    public void setKaryawanNik(String karyawanNik) {
	this.karyawanNik = karyawanNik;
    }

    public String getProjekKode() {
	return projekKode;
    }

    public void setProjekKode(String projekKode) {
	this.projekKode = projekKode;
    }

    public int getBonus() {
	return bonus;
    }

    public void setBonus(int bonus) {
	this.bonus = bonus;
    }

    public Karyawan getKaryawan() {
	return karyawan;
    }

    public void setKaryawan(Karyawan karyawan) {
	this.karyawan = karyawan;
    }

    public Projek getProjek() {
	return projek;
    }

    public void setProjek(Projek projek) {
	this.projek = projek;
    }

}
