
<%@page import="com.ukrim.model.Karyawan"%>
<%
    String nik = request.getParameter("nik");
    Karyawan kr = Karyawan.getByPrimaryKey(nik);
    if (kr == null) {
        out.println("Data yang anda cari tidak ada");
        out.println("<br/><a href='index.jsp'>Kembali</a>");
        return;
    }
//1.Ambil semua parameter
    String nama = request.getParameter("nama");
    String alamat = request.getParameter("alamat");
    String tanggalLahir = request.getParameter("tanggalLahir");
    String jenisKelamin = request.getParameter("jenisKelamin");
//2.Ciptakan objek dari mahasiswa dan set semua atributnya
    kr.setNik(nik);
    kr.setNama(nama);
    kr.setAlamat(alamat);
    kr.setTanggalLahir(tanggalLahir);
    kr.setJenisKelamin(jenisKelamin);
//3.Masukkan ke dalam MahasiswaList menggunakan method tambahData
    kr.ubahData();
//4.Kembalikan ke halaman utama
    response.sendRedirect("index.jsp");
%>