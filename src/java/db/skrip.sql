drop database pent1_2018_1;
create database pent1_2018_1;
use pent1_2018_1;

create table karyawan(
  nik varchar(5) not null primary key,
  nama varchar(20),
  jenis_kelamin enum('L','P'),
  tanggal_lahir date,
  alamat varchar(40)
);

insert into karyawan values
('K001','Sukro','L','2000-01-01','Jogja'),
('K002','Sukriwati','P','2000-01-02','Medan'),
('K003','Sukroman','L','2000-01-03','Jakarta');

drop table mobil;
create table mobil(
  plat_no char(10) not null primary key,
  merek char(30),
  warna char(30),
  karyawan_nik varchar(5) not null,
  foreign key (karyawan_nik) references karyawan(nik)
);

create table projek(
  kode char(10) not null primary key,
  nama char(100),
  biaya int
);

/* m-n dari projek dan karyawan*/
create table projek_karyawan(
  id int not null primary key auto_increment,
  karyawan_nik varhar(5) not null,
  projek_kode char(10),
  bonus int,
    foreign key (karyawan_nik) references karyawan(nik),
    foreign key (projek_kode) references projek(kode)
);
