<%@page import="com.ukrim.model.Karyawan"%>
<%
    String nik = request.getParameter("nik");
    Karyawan kr = Karyawan.getByPrimaryKey(nik);
    if (kr == null) {
        out.println("Data yang anda cari tidak ada");
        out.println("<br/><a href='index.jsp'>Kembali</a>");
        return;
    }
    kr.hapusData();
    response.sendRedirect("index.jsp");
%>