
<%@page import="com.ukrim.model.Karyawan"%>
<%
//1.Ambil semua parameter
    String nik = request.getParameter("nik");
    String nama = request.getParameter("nama");
    String alamat = request.getParameter("alamat");
    String tanggalLahir = request.getParameter("tanggalLahir");
    String jenisKelamin = request.getParameter("jenisKelamin");
//2.Ciptakan objek dari mahasiswa dan set semua atributnya
    Karyawan kr = new Karyawan();
    kr.setNik(nik);
    kr.setNama(nama);
    kr.setAlamat(alamat);
    kr.setTanggalLahir(tanggalLahir);
    kr.setJenisKelamin(jenisKelamin);
//3.Masukkan ke dalam MahasiswaList menggunakan method tambahData
    kr.tambahData();
//4.Kembalikan ke halaman utama
    response.sendRedirect("index.jsp");

%>