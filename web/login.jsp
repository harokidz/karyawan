<%-- 
    Document   : login
    Created on : Nov 5, 2018, 1:36:17 PM
    Author     : harokidz
--%>

<%@page import="com.ukrim.model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="lib/css/bootstrap.css" rel="stylesheet"/>
    </head>
    <body>
                <%
        HttpSession sess = request.getSession();
        User user = (User) sess.getAttribute("user");
        if(user != null){
            response.sendRedirect("index.jsp");
            return;
        }
        %>
        <div class="container">
            <div class="row">
                <div style="margin-top: 100px" class="col-4 offset-4 ">
                    <div class="card">
                        <div class="card-header text-white bg-dark ">
                            Login
                        </div>
                        <div class="card-body">
                            <span style="font-size: 10pt">Silahkan masukkan username dan password</span>
                            <form method="post" action="prosesLogin.jsp"> 
                                <div class="form-group">
                                    <input class="form-control" type="text" name="username" placeholder="Username" required/>
                                </div> 
                                <div class="form-group">
                                    <input class="form-control" type="password" name="password" placeholder="Password" required/>
                                </div> 
                                <div class="form-group">
                                    <input class="form-control btn btn-primary" type="submit" value="Login" />
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
