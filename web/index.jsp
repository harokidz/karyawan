<!doctype html>
<html>
    <head>
        <title>Data Karyawan</title>
        <link href="lib/css/bootstrap.css" rel="stylesheet"/>
    </head>
    <body>
        <div class="container-fluid">
            <!-- Content here -->
            <h1>Data Karyawan</h1>
            <div class="row">
                <!--setting menu-->
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header text-white bg-dark ">
                            Menu
                        </div>
                        <div class="card-body">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <a href="index.jsp">Data Karyawan</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="index.jsp?action=formTambah">Tambah Karyawan</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="index.jsp?action=listMobil">Data Mobil</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="index.jsp?action=tambahMobil">Tambah Mobil</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--setting content-->
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header text-white bg-success ">
                            Data
                        </div>
                        <div class="card-body">
                            <%
                                String action = request.getParameter("action");
                                String halaman = "karyawan/listKaryawan.jsp";
                                if (action != null) {
                                    if (action.equalsIgnoreCase("formTambah")) {
                                        halaman = "karyawan/formTambah.jsp";
                                    }
                                    else if (action.equalsIgnoreCase("formUbah")) {
                                        halaman = "karyawan/formUbah.jsp";
                                    }
                                    else if (action.equalsIgnoreCase("listMobil")) {
                                        halaman = "mobil/list.jsp";
                                    }
                                    else if (action.equalsIgnoreCase("tambahMobil")) {
                                        halaman = "mobil/formTambah.jsp";
                                    }
                                    else if (action.equalsIgnoreCase("assignProjek")) {
                                        halaman = "karyawanprojek/assignProjek.jsp";
                                    }
                                }
                            %>
                            <jsp:include page="<%=halaman%>"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="lib/jquery.js"></script>
        <script src="lib/js/bootstrap.js"></script>
    </body>
</html>