<%@page import="com.ukrim.model.Mobil"%>
<%@page import="java.util.ArrayList"%>
<table class="table table-striped">
    <tr>
        <th>Plat Nomer</th>
        <th>Merek</th>
        <th>Warna</th>
        <th>Milik Dari</th>
        <th>Action</th>
    </tr>
    <%
        ArrayList<Mobil> mobilList = Mobil.getAll();
        for (Mobil m : mobilList) {//for each
    %>
    <tr>
        <td><%=m.getPlatNo() %></td>
        <td><%=m.getMerek() %></td>
        <td style="background: <%=m.getWarna() %>"></td>
        <td><%=m.getKaryawan().getNama() %> - <%=m.getKaryawan().getAlamat() %></td>
        <td>Update Delete</td>
    </tr>
    <%
        }
    %>
</table>