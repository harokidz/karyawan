<%@page import="com.ukrim.util.Util"%>
<%@page import="com.ukrim.model.Karyawan"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="table-responsive">
    <table class="table table-striped table-hover">
        <tr>
            <th>NIK</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Jenis Kelamin</th>
            <th>Tanggal Lahir</th>
            <th>Action</th>
            
        </tr>
        <%
            ArrayList<Karyawan> kList = Karyawan.getAll();
            if (kList.size() == 0) {
                out.println("<tr><td colspan='6'>Data Belum ada</td></tr>");
            } else {
                for (int i = 0; i < kList.size(); i++) {
                    Karyawan k = kList.get(i);
        %>
        <tr>
            <td><%=k.getNik()%></td>
            <td><%=k.getNama()%></td>
            <td><%=k.getAlamat()%></td>
            <td><%=Util.konversiGendre(k.getJenisKelamin())%></td>
            <td><%=Util.konversiTanggal(k.getTanggalLahir())%></td>
            <td>
                <a class="btn btn-warning btn-sm" href="index.jsp?action=formUbah&nik=<%=k.getNik()%>">Update</a>
                <a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal<%=k.getNik()%>" href="#">Delete</a>
                <a class="btn btn-primary btn-sm" href="index.jsp?action=assignProjek&nik=<%=k.getNik()%>">Assign Projek</a>
            </td>
        </tr>        
        <div class="modal fade" id="modal<%=k.getNik()%>" tabindex="-1"
             role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Anda Yakin Hapus data ini?</h6>

                    </div>
                    <div class="modal-body">
                        <%=k.getNik()%><br/>
                        <%=k.getNama()%><br/>
                        <%=k.getAlamat()%><br/>
                        <%=Util.konversiGendre(k.getJenisKelamin())%><br/>
                        <%=Util.konversiTanggal(k.getTanggalLahir())%><br/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">
                            Batal
                        </button>
                        <a href="prosesHapus.jsp?nik=<%=k.getNik()%>" class="btn btn-danger">
                            Hapus
                        </a>
                    </div>
                </div>
            </div>
        </div> 

        <%
                }
            }
        %>
    </table>
</div>