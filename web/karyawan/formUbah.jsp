<%-- 
    Document   : formTambah
    Created on : Aug 27, 2018, 2:17:23 PM
    Author     : harokidz
--%>

<%@page import="com.ukrim.model.Karyawan"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
    </head>
    <body>
         <%
            String nik = request.getParameter("nik");
            Karyawan kr = Karyawan.getByPrimaryKey(nik);
            if (kr == null) {
                out.println("Data yang anda cari tidak ada");
                out.println("<br/><a href='index.jsp'>Kembali</a>");
                return;
            }
        %>
        <h1>Ubah Mahasiswa</h1>
        <form action="prosesUbah.jsp" method="get">
            <div class="form-group">
                Nik : <input class="form-control" type="text" value=<%=kr.getNik() %>  name="nik" readonly required/>
            </div>
            <div class="form-group">
                Nama : <input class="form-control" type="text" value=<%=kr.getNama() %>  name="nama" required/>
            </div>
            <div class="form-group">
                Alamat : <input class="form-control" type="text" value=<%=kr.getAlamat() %>  name="alamat" required/>
            </div>
            <div class="form-group">
                Tanggal lahir : <input class="form-control" type="date" value=<%=kr.getTanggalLahir() %>  name="tanggalLahir" required/>
            </div>
            <div class="form-group">
                Jenis Kelamin : 
                <select class="form-control" name="jenisKelamin">
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                </select>
            </div>
            <input type="submit" class="btn btn-success" value="Simpan Data"/>
        </form>
    </body>
</html>
