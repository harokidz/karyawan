/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ukrim.model;

import com.ukrim.conn.Koneksi;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author harokidz
 */
public class Karyawan {

    private String nik;
    private String nama;
    private String jenisKelamin;
    private String tanggalLahir;
    private String alamat;

    public String getNik() {
	return nik;
    }

    public void setNik(String nik) {
	this.nik = nik;
    }

    public String getNama() {
	return nama;
    }

    public void setNama(String nama) {
	this.nama = nama;
    }

    public String getJenisKelamin() {
	return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
	this.jenisKelamin = jenisKelamin;
    }

    public String getTanggalLahir() {
	return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
	this.tanggalLahir = tanggalLahir;
    }

    public String getAlamat() {
	return alamat;
    }

    public void setAlamat(String alamat) {
	this.alamat = alamat;
    }

    public static ArrayList<Karyawan> getAll() {
	ArrayList<Karyawan> hasil = new ArrayList<>();
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "select * from karyawan";
	PreparedStatement ps = null;
	ResultSet r = null;
	try {
	    ps = c.prepareStatement(sql);
	    r = ps.executeQuery();
	    while (r.next()) {
		//1.Ciptakan objek Karyawan
		Karyawan kr = new Karyawan();
		kr.nik = r.getString("nik");
		kr.nama = r.getString("nama");
		kr.jenisKelamin = r.getString("jenis_kelamin");
		kr.tanggalLahir = r.getString("tanggal_lahir");
		kr.alamat = r.getString("alamat");
		hasil.add(kr);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    k.tutupKoneksi(ps, r, c);
	}
	return hasil;
    }

    //get By Primary key
    public static Karyawan getByPrimaryKey(String nik) {
	Karyawan kr = null;
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "select * from karyawan where nik=?";
	PreparedStatement ps = null;
	ResultSet r = null;
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, nik);
	    r = ps.executeQuery();
	    if (r.next()) {
		kr = new Karyawan();
		kr.nik = r.getString("nik");
		kr.nama = r.getString("nama");
		kr.jenisKelamin = r.getString("jenis_kelamin");
		kr.tanggalLahir = r.getString("tanggal_lahir");
		kr.alamat = r.getString("alamat");
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    k.tutupKoneksi(ps, r, c);
	}

	return kr;
    }

    public void tambahData() {
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "insert into karyawan(nik,nama,jenis_kelamin,tanggal_lahir,alamat) "
		+ "values (?,?,?,?,?)";
	PreparedStatement ps = null;
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, this.nik);
	    ps.setString(2, this.nama);
	    ps.setString(3, this.jenisKelamin);
	    ps.setString(4, this.tanggalLahir);
	    ps.setString(5, this.alamat);
	    ps.executeUpdate();
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {

	}
    }

    public void hapusData() {
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "delete from karyawan where nik=?";
	PreparedStatement ps = null;
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, this.nik);
	    ps.executeUpdate();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public void ubahData() {
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "update karyawan set nama=?,jenis_kelamin=?,alamat=?,tanggal_lahir=? where nik=?";
	PreparedStatement ps = null;
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, this.nama);
	    ps.setString(2, this.jenisKelamin);
	    ps.setString(3, this.alamat);
	    ps.setString(4, this.tanggalLahir);
	    ps.setString(5, this.nik);
	    ps.executeUpdate();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public static void main(String[] args) {
	Karyawan kr = Karyawan.getByPrimaryKey("K004");
	kr.hapusData();
    }
}
