/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ukrim.model;

import com.ukrim.conn.Koneksi;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author harokidz
 */
public class Projek {

    private String kode;
    private String nama;
    private int biaya;

    public String getKode() {
	return kode;
    }

    public void setKode(String kode) {
	this.kode = kode;
    }

    public String getNama() {
	return nama;
    }

    public void setNama(String nama) {
	this.nama = nama;
    }

    public int getBiaya() {
	return biaya;
    }

    public void setBiaya(int biaya) {
	this.biaya = biaya;
    }

    public static ArrayList<Projek> getAll() {
	ArrayList<Projek> hasil = new ArrayList<>();
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "select * from projek";
	PreparedStatement ps = null;
	ResultSet r = null;
	try {
	    ps = c.prepareStatement(sql);
	    r = ps.executeQuery();
	    while (r.next()) {
		//1.Ciptakan objek Karyawan
		Projek kr = new Projek();
		kr.kode = r.getString("kode");
		kr.nama = r.getString("nama");
		kr.biaya = r.getInt("biaya");
		hasil.add(kr);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    k.tutupKoneksi(ps, r, c);
	}
	return hasil;
    }

    //get By Primary key
    public static Projek getByPrimaryKey(String kode) {
	Projek kr = null;
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "select * from projek where kode=?";
	PreparedStatement ps = null;
	ResultSet r = null;
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, kode);
	    r = ps.executeQuery();
	    if (r.next()) {
		kr = new Projek();
		kr.kode = r.getString("kode");
		kr.nama = r.getString("nama");
		kr.biaya = r.getInt("biaya");
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    k.tutupKoneksi(ps, r, c);
	}

	return kr;
    }

    public void tambahData() {
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "insert into projek(kode,nama,biaya) "
		+ "values (?,?,?)";
	PreparedStatement ps = null;
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, this.kode);
	    ps.setString(2, this.nama);
	    ps.setInt(3, this.biaya);
	    ps.executeUpdate();
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {

	}
    }

    public void hapusData() {
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "delete from projek where kode=?";
	PreparedStatement ps = null;
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, this.kode);
	    ps.executeUpdate();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public void ubahData() {
	Koneksi k = new Koneksi();
	Connection c = k.getKoneksi();
	String sql = "update projek set nama=?,biaya=? where kode=?";
	PreparedStatement ps = null;
	try {
	    ps = c.prepareStatement(sql);
	    ps.setString(1, this.nama);
	    ps.setInt(2, this.biaya);
	    ps.setString(3, this.kode);
	    ps.executeUpdate();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }


}
